import Vue from "vue";
import Vuex from "vuex";
import addressModule from "./modules/address";
import affiliate from "./modules/affiliate";
import appModule from "./modules/app";
import authModule from "./modules/auth";
import cartModule from "./modules/cart";
import compareList from "./modules/compareList";
import deliveryboy from "./modules/deliveryboy";
import followModule from "./modules/follow";
import recentlyViewed from "./modules/recentlyViewed";
import snackBar from "./modules/snackbar";
import wishlistModule from "./modules/wishlist";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        app: appModule,
        auth: authModule,
        address: addressModule,
        wishlist: wishlistModule,
        follow: followModule,
        cart: cartModule,
        snackbar: snackBar,
        recentlyViewed: recentlyViewed,
        compareList:compareList,
        affiliate:affiliate,
        deliveryboy:deliveryboy,
    },
});

export default store;
